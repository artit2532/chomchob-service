<?php

return [
	'service_url' => 'https://secure.thaibulksms.com/sms_api_test.php',//test
	// 'service_url' => 'https://secure.thaibulksms.com/sms_api.php',//production
	
	'test_setting' => [
		'username'  =>  'thaibulksms',
	    'password'  =>  'thisispassword',
	    'msisdn'    =>  '0955935045',//Destination Phone Number (08xxxxxxxx) and multiple number by using comma (,)
	    'sender'    =>  'TestSender',
	    'force'     =>  'standard',
	],
	'production_setting' => [
		'username'  =>  'thaibulksms',
	    'password'  =>  'thisispassword',
	    'msisdn'    =>  '0955935045',//Destination Phone Number (08xxxxxxxx) and multiple number by using comma (,)
	    'sender'    =>  'TestSender',
	    'force'     =>  'standard',
	],
];