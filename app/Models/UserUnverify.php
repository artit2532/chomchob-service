<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserUnverify extends Model
{
    protected $table = 'user_unverify';
    protected $fillable = [ 'email',
                            'first_name',
                            'last_name', 
                            'display_name', 
                            'date_of_birth', 
                            'gender' , 
                            'mobile_number', 
                            'homepage', 
                            'description',
                            'password',
                            'verify_token',];
}