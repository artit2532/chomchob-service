<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCCP extends Model
{
    protected $table = 'user_ccp';
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\Models\User','id','user_id');
    }

    
}
