<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transaction';
    protected $fillable = [ 
    	'number',
        'user_id',
        'shop_branch_id', 
        'among_ccp', 
        'net_ccp', 
        'voucher_id' , 
        'status',
    ];
    
}