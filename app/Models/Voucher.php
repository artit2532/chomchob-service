<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use User;
class Voucher extends Model
{
    protected $table = 'voucher';
    public $timestamps = false;

    public function scopeOfCode($query, $code)
    {
        return $query->where('code', $code);
    }

    public static function getVouchers($limit=50,$min_voucher_id=null){

        $query = Voucher::where('is_exclusive','0')->orderBy('id','desc');
        if ($limit>0) {
    		$query->take($request->limit);
    	}
        if($min_voucher_id)
        {
            $query->where('id','<', $min_voucher_id);
        }
        return $query->get();
    }

    public static function getExclusiveVouchers($limit=50,$min_voucher_id=null,$user_id){
    	$query = DB::table('voucher_user')->where('user_id',$user_id);
    	if ($limit>0) {
    		$query->take($request->limit)->orderBy('voucher_id','desc');
    	}
    	if($min_voucher_id) {
            $query->where('voucher_id','<', $min_voucher_id);
        }
    	$relation_exclusives = $query->get();
    	$relation_exclusive_list = [];
    	foreach ($relation_exclusives as $relation_exclusive) {
    		$relation_exclusive_list[$relation_exclusive->voucher_id] = $relation_exclusive;
    	}
        $exclusive_vouchers = Voucher::whereIn('id',array_keys($relation_exclusive_list))->orderBy('id','desc')->get();
        foreach ($exclusive_vouchers as $i => $exclusive_voucher) {
        	$id = $exclusive_voucher->id;
    		$exclusive_voucher->start_date = $relation_exclusive_list[$id]->start_date;
    		$exclusive_voucher->end_date = $relation_exclusive_list[$id]->end_date;
    	}
        return $exclusive_vouchers;
    }

    public static function getExclusiveVoucherByUserToken($limit=50,$min_voucher_id=null,$token){
    	return Voucher::getExclusiveVouchers($limit,$min_voucher_id, User::getUserIDByToken($token));
    }

}
