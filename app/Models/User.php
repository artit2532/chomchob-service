<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Carbon\Carbon;
class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'email',
                            'first_name',
                            'last_name', 
                            'display_name', 
                            'date_of_birth', 
                            'gender' , 
                            'mobile_number', 
                            'homepage', 
                            'description',
                            'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token','pin_password','facebook_token' ,'payment_token','created_at','updated_at'];

    public function scopeOfToken($query, $token)
    {
        return $query->where('remember_token', $token);
    }

    public function scopeOfPaymentToken($query, $payment_token)
    {
        return $query->where('payment_token', $payment_token)->where('payment_token_expire',"<",Carbon::now()->toDateTimeString());
    }

    public static function createNewUser(array $params)
    {
        $user = User::create($params);
        if (!$user) {
            return false;
        }
        return $user->ccp()->save(new UserCCP());
    }

    public function ccp()
    {
        return $this->hasOne('App\Models\UserCCP','user_id','id');
    }

    public static function getUserIDByToken($token){
        $user = User::ofToken($token)->select('id')->first();
        if (!$user ) {
            return false;
        }
        return $user->id;
    }

    public static function getUserByToken($token){
        return User::ofToken($token)->first();
    }

    public static function getUserByPaymentToken($payment_token){
        return User::ofPaymentToken($payment_token)->first();
    }

    
}
