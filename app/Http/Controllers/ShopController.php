<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ShopController extends Controller
{
    public function search(Request $request)
    {
        $validateResult = $this->validateInput($request->all(),[
            // 'token' => 'required|exists:user,remember_token',
            'search_text' => 'required',
            'limit' => 'sometimes|integer|min:1',
            'skip'  => 'sometimes|integer|min:1',
        ]);
        if (!$validateResult) {
            return response()->json($this->responseObject);
        }
        // DB::enableQueryLog();
        $query = Shop::skip($request->input('skip',0))
                    ->take($request->input('limit',PHP_INT_MAX))
                    ->where(function ($local_query) use ($request){
                        $local_query->where('name_en',"LIKE","%".$request->search_text."%")
                                    ->orWhere('name_th',"LIKE","%".$request->search_text."%");
                    });
        if ($request->has('limit')) {
            $query->take($request->limit);
        }

        $shop_list = $query->get();

        return response()->json($this->getResponseObject(true,null,[   
            'shop_list'=>$shop_list,
            // 'queryLog'=>DB::getQueryLog(),
        ]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
