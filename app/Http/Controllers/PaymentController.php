<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use App\Models\User;
use App\Models\UserCCP;
use App\Models\ShopBranch;
use App\Models\Voucher;
use App\Models\Transaction;
use DB;
require app_path().'/Helper/SMS.php';
class PaymentController extends Controller
{
    public function generatePaymentToken($user_id){
        $payment_token = uniqid(md5($user_id),true);
        if (User::ofPaymentToken($payment_token)->exists()) {
            return $this->generatePaymentToken($prefix,$more_entropy);
        }
        return $payment_token;
    }

    public function saveNewPinPassword(Request $request){
        $validateResult = $this->validateInput($request->all(),[
            'token' => 'required|exists:user,remember_token',
            'new_pin_password' => 'required|min:6|max:6|regex:/[0-9]*/',
            'password' => 'required|min:8|max:16|regex:/^[A-Za-z0-9@!#\$%\^&\*]+$/',
        ]);
        if (!$validateResult) {
            return response()->json($this->responseObject);
        }

        $user = User::ofToken($request->token)->first();
        if (!Hash::check($request->password, $user->password)) {
            return response()->json($this->getResponseObject(false,'The password is incorrect.'));
        }
        else if (!empty($user->pin_password)) {
            return response()->json($this->getResponseObject(false,"You don't have permission to save new pin. Please contact chomchob."));
        }

        $result = User::ofToken($request->token)->update([  
            'pin_password'  =>  Hash::make($request->new_pin_password),
            'payment_token' =>  "",
            'payment_token_expire' => Carbon::now()->toDateTimeString(),
        ]);
        if(!$result){
            return response()->json($this->getResponseObject(false,"Cannot save new pin password please contact chomchob."));
        }
        return response()->json($this->getResponseObject(true,''));
    }

    public function changePinPassword(Request $request){
        $validateResult = $this->validateInput($request->all(),[
            'token' => 'required|exists:user,remember_token',
            'old_pin_password' => 'required|min:6|max:6|regex:/[0-9]*/',
            'new_pin_password' => 'required|min:6|max:6|regex:/[0-9]*/',
            'password' => 'required|min:8|max:16|regex:/^[A-Za-z0-9@!#\$%\^&\*]+$/',
        ]);
        if (!$validateResult) {
            return response()->json($this->responseObject);
        }

        $user = User::ofToken($request->token)->first();
        if (!Hash::check($request->old_pin_password, $user->pin_password) && !Hash::check($request->password, $user->password)) {
            return response()->json($this->getResponseObject(false,'The password or pin password is incorrect.'));
        }
        $result = User::ofToken($request->token)->update([  
            'pin_password'  =>  Hash::make($request->new_pin_password),
            'payment_token' =>  "",
            'payment_token_expire' => Carbon::now()->toDateTimeString(),
        ]);
        if(!$result){
            return response()->json($this->getResponseObject(false,"Cannot save new pin password please contact chomchob."));
        }
        return response()->json($this->getResponseObject(true,''));
    }

    public function resetPinPassword(Request $request){
        $validateResult = $this->validateInput($request->all(),[
            'token' => 'required|exists:user,remember_token',
            'password' => 'required|min:8|max:16|regex:/^[A-Za-z0-9@!#\$%\^&\*]+$/',
        ]);
        if (!$validateResult) {
            return response()->json($this->responseObject);
        }

        $user = User::ofToken($request->token)->first();
        if (!Hash::check($request->password, $user->password)) {
            return response()->json($this->getResponseObject(false,'Password is incorrect.'));
        }

        $result = User::ofToken($request->token)->update([  
            'pin_password'  =>  "",
            'payment_token' =>  "",
            'payment_token_expire' => Carbon::now()->toDateTimeString(),
        ]);
        if(!$result){
            return response()->json($this->getResponseObject(false,"Cannot reset pin password please contact chomchob."));
        }

        return response()->json($this->getResponseObject(true,''));
    }

    public function paymentLogin(Request $request){
        $validateResult = $this->validateInput($request->all(),[
            'token' => 'required|exists:user,remember_token',
            'pin_password' => 'required|min:6|max:6|regex:/[0-9]*/',
        ]);
        if (!$validateResult) {
            return response()->json($this->responseObject);
        }

        $user = User::ofToken($request->token)->first();
        if (!Hash::check($request->pin_password, $user->pin_password)){
            return response()->json($this->getResponseObject(false,'The pin password is incorrect.'));
        }

        //reset payment_token every time when payment login
        $user->payment_token = $this->generatePaymentToken();
        $user->payment_token_expire = Carbon::now()->addMinutes(5)->toDateTimeString();
        
        if(!$user->save()){
            return response()->json($this->getResponseObject(false,'Cannot login to payment section please contact chomchob.'));
        }

        return response()->json($this->getResponseObject(true,'',['payment_token'=>$user->payment_token]));
    }

    public function payToMerchant(Request $request){
        $validateResult = $this->validateInput($request->all(),[
            'payment_token' => 'required|exists:user,payment_token',
            'shop_branch_id' => 'required|exists:shop_branch,id',
            'voucher_code' => 'sometimes|exists:voucher,code',
            'net_ccp' => 'required|integer',
            'among_ccp' => 'required|integer',
            'user_current_ccp' => 'required|integer',
        ]);

        if (!$validateResult) {
            return response()->json($this->responseObject);
        }

        //check payment token
        $user = User::getUserByPaymentToken($request->payment_token);
        if (!$user) {
            return response()->json($this->getResponseObject(false,'Please login payment again'));
        }
        //check if user ccp not enough
        else if ($request->user_current_ccp < $request->net_ccp) {
            return response()->json($this->getResponseObject(false,'Not enough ccp for pay.'));
        }

        //check shop is ready before pay
        $shop_branch = ShopBranch::find(intval($request->shop_branch_id));
        if (empty($shop_branch->bank_id) || empty($shop_branch->bank_account)) {
            return response()->json($this->getResponseObject(false,'This shop is not set bank account'));
        }

        //check discount from voucher
        if ($request->has('voucher_code')) {
            $voucher = Voucher::ofCode($request->code)->first();

            //check user not use voucher exceed the global limit
            //$voucher->quota_quantity = 0 mean that voucher has no limit use but maybe has limit per user use
            if ($voucher->quota_quantity == 0) {
                $voucher_used_quantity = Transaction::where('voucher_id',$voucher->id)->where('status','!=','void')->count();
                if ($voucher_used_quantity >= $voucher->quota_quantity) {
                    return response()->json($this->getResponseObject(false,'Voucher have been used to exceed the limit.'));
                }
            }
            //check user not use voucher exceed the limit per user
            $voucher_used_per_user_quantity = Transaction::where('voucher_id',$voucher->id)
                                                        ->where('status','!=','void')
                                                        ->where('user_id',$user->id)
                                                        ->count();
            if ($voucher_used_per_user_quantity >= $voucher->limit_per_user) {
                return response()->json($this->getResponseObject(false,'Voucher have been used to exceed the limit per user.'));
            }

            //check discount is correct
            if ($voucher->discount_unit=='percent' && $voucher->discount_value > 0) { //if discount by percent
                $discount = ($voucher->discount_value * $request->among_ccp)/100;
                $discount = round($discount);
            }
            else if($voucher->discount_unit=='value'){ //if discount by value
                $discount = $voucher->discount_value;
            }
            else if($voucher->discount_unit=='no_discount'){ //if discount by value
                $discount = 0;
            }
            else{
                return response()->json($this->getResponseObject(false,'Not found this discount unit type please contact chomchob.'));
            }

            $net_ccp = $request->among_ccp - $discount;
            if ($net_ccp<0) {
                $net_ccp = 0;
            }

            if ($request->net_ccp != $net_ccp) {
                return response()->json($this->getResponseObject(false,'Discount is missmatch with server please contact chomchob.'));
            }
        }

        $transaction_info = [
            'user_id' => $user->id,
            'shop_branch_id' => $shop_branch->id,
            'among_ccp' => $request->among_ccp,
            'net_ccp' => $request->net_ccp,
            'status' => 'pending',
        ];

        if (isset($voucher)) {
            $transaction_info['voucher_id'] = $voucher->id;
        }
        $now = Carbon::now();
        $transaction_today_number = $now->format('Ymd')."00000";
        $transaction_number = Transaction::max('number');
        if (empty($transaction_number) || $transaction_number < $transaction_today_number) {
            $transaction_number = $transaction_today_number;
        }

        $commit_transaction = false;
        //begin transaction
        DB::beginTransaction();

        if(!UserCCP::where('user_id',$user->id)
                ->where('ccp',$request->user_current_ccp)
                ->decrement('ccp',$request->net_ccp)){
            return response()->json($this->getResponseObject(false,'Client data not updated please try again.')); 
        }

        for ($i=0; $i < 10; $i++) { 
            try {
                $transaction_info['number'] = ++$transaction_number;
                $transaction = Transaction::create($transaction_info);
                // all good
                DB::commit();
                $commit_transaction = true;
                break;
            } catch (\Illuminate\Database\QueryException $e){
                DB::rollback();
                $errorCode = $e->errorInfo[1];
                if($errorCode == 1062){
                    // houston, we have a duplicate entry problem
                    echo $e->getMessage();
                    continue;
                }
            }

        }
        if (!$commit_transaction) {
            return response()->json($this->getResponseObject(false,'Cannot complete transaction this time please try again later.'));
        }
        $text = $now->format('HH:mm')." ".$user->display_name." จ่าย ";
        if (isset($discount)) {
            $text .= $request->among_ccp." ส่วนลด $discount เหลือสุทธิ ";
        }
        $text .= $request->net_ccp." CCP";

        sendSMS($text,$shop_branch->sms_phone_number);
        
        return response()->json($this->getResponseObject(true,'Transaction success',['transaction_number'=>$transaction->number]));
    }

    public function test(){
    }

}
