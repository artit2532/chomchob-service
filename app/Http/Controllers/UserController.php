<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserUnverify;
use Hash;
use DB;
use Config;
use Storage;
use Mail;
class UserController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json($this->getResponseObject());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function generateToken($prefix="",$more_entropy=false){
        $token = uniqid();
        if (User::ofToken($token)->exists()) {
            return $this->generateToken($prefix,$more_entropy);
        }
        return $token;
    }

    public function randomString($number=10){
        return substr(md5(microtime()),rand(0,26),$number);
    }

    public function checkEmail(Request $request){
        //check user_unverify 
        $validateResult = $this->validateInput($request->all(),[
            'email' => 'required|email',
        ]);
        if (!$validateResult) {
            return response()->json($this->responseObject);
        }
        else if (User::where('email',$request->email)->exists()) {
            return response()->json($this->getResponseObject(true,'This email is registered and verified',['email_status'=>2]));
        }
        else if (UserUnverify::where('email',$request->email)->exists()) {
            return response()->json($this->getResponseObject(true,'This email is registered but not verify yed.',['email_status'=>1]));
        }
        return response()->json($this->getResponseObject(true,'This email is not register',['email_status'=>0]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        //check user_unverify 
        $validateResult = $this->validateInput($request->all(),[
            'email' => 'email|unique:user_unverify,email',
        ],['email.unique' => 'this email has already been used']);

        if (!$validateResult) {
            return response()->json($this->responseObject);
        }

        //check verify user
        $validateResult = $this->validateInput($request->all(),[
            'email' => 'required|unique:user,email|email',
            'first_name' => 'sometimes|max:128|alpha',
            'last_name' => 'sometimes|max:128|alpha',
            'display_name' => 'required|unique:user,display_name|max:20',
            'password' => 'required|min:8|max:16|regex:/^[A-Za-z0-9@!#\$%\^&\*]+$/',
            'date_of_birth' => 'sometimes|date_format:Y-m-d',
            'gender' => 'sometimes|in:male,female',
            'profile_picture' => 'sometimes|mimes:jpeg,png',
            'mobile_number' => 'sometimes|min:3|max:20|regex:/[0-9]*/',
            'homepage' => 'sometimes|string',
            'description' => 'sometimes|string'
        ],['email.unique' => 'this email has already been used']);

        if (!$validateResult) {
            return response()->json($this->responseObject);
        }

        $request->merge(array('password' => Hash::make($request->password)));

        if ($request->hasFile('profile_picture')) {
            $profile_picture = $request->file('profile_picture');
            if ($profile_picture->isValid()) {
                $extension = $profile_picture->getClientOriginalExtension();
                $profile_picture_name = randomString().".$extension";
                $save_result = $profile_picture->move(Config::get('filesystems.profile_picture_path').$profile_picture_name);
                if (!$save_result) {
                    return response()->json($this->getResponseObject(false,"Cannot save profile picture please contact chomchob."));
                }
                $request->merge(array('profile_picture' => $profile_picture_name));
            }
        }

        //gen verify token
        $token = uniqid('',true);
        while (UserUnverify::where('verify_token',$token)->exists()) {
            $token = uniqid('',true);
        }
        $request->merge(array('verify_token' => $token));

        //create user in unverify table
        $unverify_user = UserUnverify::create($request->all());

        if (!$unverify_user) {
            return response()->json($this->getResponseObject(false,"Cannot create new user please contact chomchob."));
        }

        $this->sendVerifyEmail($unverify_user->email,"{$unverify_user->first_name} {$unverify_user->last_name}",$token);


        return response()->json($this->getResponseObject());
    }

    public function sendVerifyEmail($sendto,$sendto_name,$verify_token){
        Mail::queue('emails.askconfirm', ['confirm_link' => action('UserController@verifyEmail',['verify_code' => $verify_token])], function($message) use ($sendto,$sendto_name){
          $message->to($sendto, $sendto_name)->subject('Chomchob email verification.');
        });
    }

    public function verifyEmail(Request $request,$verify_code = null){
        if (empty($verify_code)) {
            //return fail view
            return response()->json($this->getResponseObject(false,'Invalid verify link.'));
        }
        $unverify_user = UserUnverify::where('verify_token',$request->verify_code)->where('is_verify',0)->first();

        if (!$unverify_user) {
            return response()->json($this->getResponseObject(false,'Invalid verify link.'));
        }

        $attributes = $unverify_user->getAttributes();
        if(!User::createNewUser($attributes)){
            return response()->json($this->getResponseObject(false,'Some error has occur in server please contact chomchob.')); 
        }
        $unverify_user->is_verify = 1;
        $unverify_user->save();
        return view('emails.thankyouconfirm');
    }

    public function login(Request $request){
        $validateResult = $this->validateInput($request->all(),[
            'email' => 'required|email',
            'password' => 'required|min:8|max:16|regex:/^[A-Za-z0-9@!#\$%\^&\*]+$/'
        ]);
        if (!$validateResult) {
            return response()->json($this->responseObject);
        }

        $user = User::where('email',$request->email)->first();
        if (!$user) {
            if (UserUnverify::where('email',$request->email)->exists()) {
                return response()->json($this->getResponseObject(false,'User is not verified'));
            }
            return response()->json($this->getResponseObject(false,'Cannot login maybe email or password is incorrect.'));
        }
        else if (!Hash::check($request->password, $user->password)){
            return response()->json($this->getResponseObject(false,'Cannot login maybe email or password is incorrect.'));
        }

        //if not have token we will create one
        if(empty($user->remember_token)){
            $user->remember_token = $this->generateToken();
        }
        
        if(!$user->save()){
            return response()->json($this->getResponseObject(false,'Cannot login please contact chomchob.'));
        }

        return response()->json($this->getResponseObject(true,'',['token'=>$user->remember_token,'user'=>$user]));
    }

    public function editProfile(Request $request){
        $validateResult = $this->validateInput($request->all(),[
            'token' => 'required|exists:user,remember_token',
            'first_name' => 'sometimes|max:128|alpha',
            'last_name' => 'sometimes|max:128|alpha',
            'display_name' => 'required|unique:user,display_name|max:20',
            'date_of_birth' => 'sometimes|date_format:Y-m-d',
            'gender' => 'sometimes|in:male,female',
            'profile_picture' => 'sometimes|mimes:jpeg,png',
            'mobile_number' => 'sometimes|min:3|max:20|regex:/[0-9]*/',
            'homepage' => 'sometimes|string',
            'description' => 'sometimes|string',
        ]);
        if (!$validateResult) {
            return response()->json($this->responseObject);
        }

        if ($request->hasFile('profile_picture')) {
            $profile_picture = $request->file('profile_picture');
            if ($profile_picture->isValid()) {
                $extension = $profile_picture->getClientOriginalExtension();
                $profile_picture_name = randomString().".$extension";
                $save_result = $profile_picture->move(Config::get('filesystems.profile_picture_path').$profile_picture_name);
                if (!$save_result) {
                    return response()->json($this->getResponseObject(false,"Cannot save profile picture please contact chomchob."));
                }
                $request->merge(array('profile_picture' => $profile_picture_name));
                $user = User::ofToken($request->token)->select('profile_picture')->first();
                if (!empty($user->profile_picture) && Storage::has(Config::get('filesystems.profile_picture_path').$user->profile_picture)) {
                    Storage::delete(Config::get('filesystems.profile_picture_path').$user->profile_picture);
                }
            }
        }

        $result = User::ofToken($request->token)->update($request->all());

        if (!$result) {
            return response()->json($this->getResponseObject(false,"Cannot update profile please contact chomchob."));
        }

        return response()->json($this->getResponseObject());
    }

    public function changePassword(Request $request){
        $validateResult = $this->validateInput($request->all(),[
            'token' => 'required|exists:user,remember_token',
            'old_password' => 'required|min:8|max:16|regex:/^[A-Za-z0-9@!#\$%\^&\*]+$/',
            'new_password' => 'required|min:8|max:16|regex:/^[A-Za-z0-9@!#\$%\^&\*]+$/',
        ]);
        if (!$validateResult) {
            return response()->json($this->responseObject);
        }
        $user = User::ofToken($request->token)->first();
        if (!Hash::check($request->old_password, $user->password)) {
            return response()->json($this->getResponseObject(false,'Password is incorrect.'));
        }

        $user->password = Hash::make($request->new_password);
        $user->remember_token = $this->generateToken();
        $user->payment_token = "";
        if($user->save()){
            return response()->json($this->getResponseObject(false,"Cannot save new password please contact chomchob."));
        }
        return response()->json($this->getResponseObject(true,'',['token' => $user->remember_token]));
    }

    public function enableMerchant(){
        $validateResult = $this->validateInput($request->all(),[
            'token' => 'required|exists:user,remember_token',
        ]);
        if (!$validateResult) {
            return response()->json($this->responseObject);
        }
        else if(!User::ofToken($token)->update(["is_merchant_enable"=>1])){
            return response()->json($this->getResponseObject(false,"Cannot update enable please contact chomchob."));
        }
        return response()->json($this->getResponseObject());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($token)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
}
