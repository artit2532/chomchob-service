<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Validator;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $responseObject;

    function __construct() {
       $responseObject = ['status' => 0];
    }

    protected function setResponseMessage($message=null){
        if ($message) 
            $this->responseObject['message'] = $message;
        return $this->responseObject;
    }

    protected function addResponseParams($additionalParams=[]){
        $this->responseObject = array_merge($this->responseObject,$additionalParams);
        return $this->responseObject;
    }
    
    protected function getResponseObject($status=true,$message=null,$additionalParams=[]){
    	if (!$status) {
    		$this->responseObject['status'] = 0;
    	}
    	else
    		$this->responseObject['status'] = 1;

    	if ($message) 
            $this->setResponseMessage($message);
    	
    	return $this->addResponseParams($additionalParams);
    }

    protected function validateInput($inputs = [],$rules = [],$customMessages = []){
        $validator = Validator::make($inputs, $rules , $customMessages);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $this->getResponseObject(false,'Some input is incorrect.',['input_error' => $messages]);
            return false; 
        }
        return true;
    }
}
