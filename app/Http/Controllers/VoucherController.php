<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Voucher;
use DB;
class VoucherController extends Controller
{
    public function getVoucher(Request $request)
    {
        $validateResult = $this->validateInput($request->all(),[
            'token' => 'required|exists:user,remember_token',
            'limit' => 'integer',
            'min_voucher_id' => 'exists:voucher,id',
        ]);

        if (!$validateResult) {
            return response()->json($this->responseObject);
        }
        $limit = $request->input('limit', '50');
        $min_voucher_id = $request->input('min_voucher_id',null);
        $vouchers = Voucher::getVouchers($limit,$min_voucher_id);
        return response()->json($this->getResponseObject(true,null,['voucher_list' => $vouchers]));
    }

    public function getExclusiveVouchers(Request $request)
    {
        $validateResult = $this->validateInput($request->all(),[
            'token' => 'required|exists:user,remember_token',
            'limit' => 'integer',
            'min_voucher_id' => 'exists:voucher,id',
        ]);

        if (!$validateResult) {
            return response()->json($this->responseObject);
        }
        $limit = $request->input('limit', '50');
        $min_voucher_id = $request->input('min_voucher_id',null);
        $vouchers = Voucher::getExclusiveVouchers($limit,$min_voucher_id,$request->token);
        return response()->json($this->getResponseObject(true,null,['exclusive_voucher_list' => $vouchers]));
    }

    public function search(Request $request)
    {
        $validateResult = $this->validateInput($request->all(),[
            // 'token' => 'required|exists:user,remember_token',
            'search_text' => 'required',
            'limit' => 'sometimes|integer|min:1',
            'skip' => 'sometimes|integer|min:1',
            'min_voucher_id' => 'sometimes|integer|exists:voucher,id',
            
        ]);
        if (!$validateResult) {
            return response()->json($this->responseObject);
        }
        // DB::enableQueryLog();
        $query = DB::table('voucher')->orderBy('voucher.id','desc')
                                    ->skip($request->input('skip',0))
                                    ->take($request->input('limit',PHP_INT_MAX))
                                    ->leftJoin('shop','voucher.shop_id','=','shop.id')
                                    ->select('voucher.*', 'shop.name_en', 'shop.name_th','shop.logo_picture');
        if ($request->has('min_voucher_id')) {
            $query->where('voucher.id','<',$request->min_voucher_id);
        }

        if (preg_match('/\d+%/', $request->search_text)) {
            $query->where('voucher.discount_unit','percent');
            $query->where('voucher.discount_value',str_replace('%', '', $request->search_text));
        }
        //check is percentage
        else if (preg_match('/\d+/', $request->search_text)) {
            $query->where('voucher.discount_value',intval($request->search_text));
        }
        else{
            $query->where(function ($local_query) use ($request){
                $local_query->where('shop.name_en',"LIKE","%".$request->search_text."%")
                            ->orWhere('shop.name_th',"LIKE","%".$request->search_text."%");
            });
        }
        $voucher_list = $query->get();

        return response()->json($this->getResponseObject(true,null,[   
            'voucher_list'=>$voucher_list,
            // 'queryLog'=>DB::getQueryLog(),
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
