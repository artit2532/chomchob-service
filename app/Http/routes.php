<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::group(['prefix' => 'api'], function () {
	Route::group(['prefix' => 'user'], function () {
		Route::post('register', 'UserController@register');
	    Route::post('login', 'UserController@login');
	    Route::post('edit', 'UserController@editProfile');
	    Route::post('check_email', 'UserController@checkEmail');
	    Route::post('password/change', 'UserController@changePassword');
	    Route::get('verify_email', 'UserController@verifyEmail');
	});

	Route::group(['prefix' => 'voucher'], function () {
		Route::post('get', 'VoucherController@getVoucher');
	});

	Route::group(['prefix' => 'payment'], function () {
		Route::post('/', 'PaymentController@payToMerchant');
	});

	Route::group(['prefix' => 'voucher'], function () {
		Route::post('search', 'VoucherController@search');
	});

	Route::group(['prefix' => 'shop'], function () {
		Route::post('search', 'ShopController@search');
	});
});


Route::get('',function(){
	echo 'base';
});

Route::get('test',function(){
	echo 'asdfsdf ss';
});