<?php
// My common functions
function sendSMS($message,$destination_phone_numbers)
{
	if (is_array($destination_phone_numbers)) {
		$destination_phone_numbers = implode(',', $destination_phone_numbers);
	}
	else if (!is_string($destination_phone_numbers)) {
		return false;
	}

    $post_data = Config::get('sms.defult_setting');
    $post_data['message'] = $message;
    $post_data['msisdn'] = $destination_phone_numbers;

    $curl = new \Ixudra\Curl\CurlService();
    $response_xml = $curl->to('https://secure.thaibulksms.com/sms_api_test.php')
					    ->withData($post_data)
					    ->withOption('SSL_VERIFYPEER', true)
					    ->post();

    if (strpos('<Status>1</Status>',$response_xml)!==false) {
        return false;
    }
    return true;
}

?>